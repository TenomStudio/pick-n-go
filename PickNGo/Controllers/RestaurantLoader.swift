//
//  RestaurantLoader.swift
//  PickNGo
//
//  Created by PC Chan on 28/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import Firebase

class RestaurantLoader {
    
    var ref: FIRDatabaseReference!
    let storage = FIRStorage.storage()
    var storageRef: FIRStorageReference!
    
    static let sharedLoader: RestaurantLoader = RestaurantLoader()
    
    private init() {
        self.ref = FIRDatabase.database().reference()
        self.storageRef = storage.referenceForURL(Constants.firebaseStorageUrl)
    }

    func loadImageTask(imageURL: NSURL, completionBlock: ((image: UIImage!, error: NSError?) -> Void)?) -> NSURLSessionDataTask {
        
        var session: NSURLSession!
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        session = NSURLSession(configuration: config)
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: imageURL)
        urlRequest.cachePolicy = NSURLRequestCachePolicy.ReturnCacheDataElseLoad
        
        let task: NSURLSessionDataTask = session.dataTaskWithRequest(urlRequest) {(imageData: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            var image: UIImage! = nil
            if let imageData = imageData {
                image = UIImage(data: imageData)
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completionBlock?(image: image, error: error)
            })
        }
        return task
    }
    
    func readRestaurantsFromServer(completionBlock: ((restaurants: [Restaurant], error: NSError?) -> Void)?) {
        
        var restaurants: [Restaurant] = []
        ref.child("restaurants")
            .queryOrderedByChild("isDeleted")
            .queryEqualToValue("false")
            .observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
                
                for item in snapshot.children {
                    let restaurant = Restaurant(snapshot: item as! FIRDataSnapshot)
                    restaurants.append(restaurant)
                }
                completionBlock?(restaurants: restaurants, error: nil)

            }, withCancelBlock: { (error) in
                completionBlock?(restaurants: [], error: error)
            })
    }
    
    
    func uploadRestaurantToServer(restaurant: Restaurant, completionBlock: ((Restaurant: Restaurant, error: NSError?) -> Void)?) {
        
        var compressedImage: UIImage!
        var data: NSData!
        
        if restaurant.photo!.size.width > 500 {
            compressedImage = restaurant.photo!.resizeWithPercentage(0.1)!
            data = UIImageJPEGRepresentation(compressedImage, 0.9)!
        } else {
            data = UIImageJPEGRepresentation(restaurant.photo!, 1.0)
        }
        
        if let user = FIRAuth.auth()?.currentUser {
            // Generate unique key for restaurants
            let key = ref.child("restaurants").childByAutoId().key
            
            // Create firebase storage reference based on image name
            let imagesRef = self.storageRef.child("images/restaurants/\(key).jpg")
            
            // Populate data into the ref
            let _ = imagesRef.putData(data, metadata: nil) { metadata, error in
                
                // If error, print error description
                if (error != nil) {
                    print(error?.localizedDescription)
                } else {
                    
                    // On added image, get image url and add into database
                    let restaurantImage = ["name": restaurant.name,
                                           "url": metadata!.downloadURL()!.absoluteString,
                                           "category": restaurant.category ?? "",
                                           "address": restaurant.location ?? "",
                                           "isDeleted": "false",
                                           "foods": restaurant.foods]
                    
                    // Add restaurant into restaurant object and userObject
                    let childUpdates = ["/restaurants/\(key)": restaurantImage,
                                        "/userObject/\(user.uid)/\(key)": restaurantImage]
                    self.ref.updateChildValues(childUpdates)
                    
                    restaurant.id = key
                    restaurant.url = metadata!.downloadURL()!.absoluteString
                    
                    completionBlock?(Restaurant: restaurant, error: nil)
                    
                    
                }
            }
        }
    }
    
    func deleteRestaurantFromServer(restaurant: Restaurant) {
        
        if let user = FIRAuth.auth()?.currentUser {
            
            let restaurantObj = ["name": restaurant.name,
                                 "url": restaurant.url!,
                                 "category": restaurant.category ?? "",
                                 "address": restaurant.location ?? "",
                                 "isDeleted": "true",
                                 "foods": restaurant.foods]
            
            let childUpdates = ["/restaurants/\(restaurant.id!)": restaurantObj,
            "/userObject/\(user.uid)/\(restaurant.id)": restaurantObj]
            
            self.ref.updateChildValues(childUpdates)
        }
    }
    
    func updateRestaurantToServer(restaurant: Restaurant) {
        
        var compressedImage: UIImage!
        var data: NSData!
        
        if restaurant.photo!.size.width > 500 {
            compressedImage = restaurant.photo!.resizeWithPercentage(0.1)!
            data = UIImageJPEGRepresentation(compressedImage, 0.9)!
        } else {
            data = UIImageJPEGRepresentation(restaurant.photo!, 1.0)
        }
        
        
        if let user = FIRAuth.auth()?.currentUser {
            
            // Create firebase storage reference based on image name
            let imagesRef = self.storageRef.child("images/restaurants/\(restaurant.id).jpg")
            
            // Populate data into the ref
            let _ = imagesRef.putData(data, metadata: nil) { metadata, error in
                
                // If error, print error description
                if (error != nil) {
                    print(error?.localizedDescription)
                } else {
                    
                    // On added image, get image url and add into database
                    let restaurantImage = ["name": restaurant.name,
                                           "url": metadata!.downloadURL()!.absoluteString,
                                           "category": restaurant.category ?? "",
                                           "address": restaurant.location ?? "",
                                           "isDeleted": "false",
                                           "foods": restaurant.foods]
                    
                    // Add restaurant into restaurant object and userObject
                    let childUpdates = ["/restaurants/\(restaurant.id!)": restaurantImage,
                                        "/userObject/\(user.uid)/\(restaurant.id!)": restaurantImage]
                    self.ref.updateChildValues(childUpdates)
                }
            }


        }
    }
    
}
