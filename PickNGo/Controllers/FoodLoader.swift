//
//  FoodLoader.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 27/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import Firebase

class FoodLoader {
    
    static let sharedLoader: FoodLoader = FoodLoader()
    var ref: FIRDatabaseReference!
    
    private init() {
        self.ref = FIRDatabase.database().reference()
    }

    func loadImageTask(imageURL: NSURL, completionBlock: ((image: UIImage!, error: NSError?) -> Void)?) -> NSURLSessionDataTask {
        
        var session: NSURLSession!
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        session = NSURLSession(configuration: config)
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: imageURL)
        urlRequest.cachePolicy = NSURLRequestCachePolicy.ReturnCacheDataElseLoad
        
        let task: NSURLSessionDataTask = session.dataTaskWithRequest(urlRequest) {(imageData: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            var image: UIImage! = nil
            if let imageData = imageData {
                image = UIImage(data: imageData)
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completionBlock?(image: image, error: error)
            })
        }
        return task
    }
    
    func readFoodsFromServer(completionBlock: ((foods: [Food], error: NSError?) -> Void)?) {
        var foods: [Food] = []
        ref.child("foods").observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                let food: Food = Food(name: rest.value!["name"] as! String)!
                food.url = rest.value!["url"] as? String
                food.maxPrice = rest.value!["price"] as? Float
                foods.append(food)
            }
            
            completionBlock?(foods: foods, error: nil)

        })
        
    }
    
        
}
