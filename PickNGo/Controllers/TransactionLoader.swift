//
//  TransactionLoader.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 10/08/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import Firebase

class TransactionLoader {

    static let service = TransactionLoader()
    var ref: FIRDatabaseReference!
    private init() {
        self.ref = FIRDatabase.database().reference()
    }
    
    func addOrderedItem(transaction: Transaction) {
        if let user = FIRAuth.auth()?.currentUser {
            let key = ref.child("transactions").childByAutoId().key
            
            let transactionObject = ["buyer": transaction.buyer! ?? "",
                                     "foodName": transaction.foodName! ?? "",
                                     "quantity": transaction.quantity!,
                                     "price": transaction.price!,
                                     "orderDate": String(transaction.orderDate!)]
            
            // Add restaurant into restaurant object and userObject
            let childUpdates = ["/transactions/\(key)": transactionObject,
                                "/userObject/\(user.uid)/transaction/\(key)": transactionObject]
            self.ref.updateChildValues(childUpdates)
        }
    }
    
    func getOrderedItem(completionBlock: ((transactions: [Transaction], error: NSError?) -> Void)?) {
        
        guard let userId = FIRAuth.auth()?.currentUser?.uid else {return}
        var transactions: [Transaction] = []
        ref.child("/userObject/\(userId)/transaction")
            .queryOrderedByKey()
            .observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
                
                for item in snapshot.children {
                    let transaction = Transaction(snapshot: item as! FIRDataSnapshot)
                    transactions.append(transaction)
                }
                transactions.sortInPlace({ $0.orderDate!.compare($1.orderDate!) == .OrderedDescending })
                completionBlock?(transactions: transactions, error: nil)
                
            }, withCancelBlock : { (error) in
                completionBlock?(transactions: [], error: error)
            })
    }

}