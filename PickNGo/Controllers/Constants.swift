//
//  Constants.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 30/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation

struct Constants {
    static let firebaseStorageUrl = "gs://pickngo-3853e.appspot.com"
}