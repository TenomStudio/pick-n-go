//
//  AddNewRestaurantViewController.swift
//  PickNGo
//
//  Created by Macintosh on 26/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit
import Firebase

protocol AddRestaurantDelegate {
    func viewController(vc: AddNewRestaurantViewController, didAddRestaurant restaurant: Restaurant!)
}

class AddNewRestaurantViewController: UIViewController,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var category: UITextField!
    @IBOutlet weak var reviewScore: UITextField!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
  
    @IBOutlet weak var image: UIImageView!
    
    var restaurant : Restaurant?
    var delegate: AddRestaurantDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let _ = self.restaurant?.id {
            
            self.name.text = self.restaurant?.name
            self.category.text = self.restaurant?.category
            self.address.text = self.restaurant?.location
            var loadImageTask: NSURLSessionDataTask?
            let url = NSURL(string: self.restaurant!.url!)!
            loadImageTask?.cancel()
            loadImageTask = RestaurantLoader.sharedLoader.loadImageTask(url, completionBlock: { (image, error) in
                if let image = image {
                    self.image.image = image
                }
            })
            loadImageTask?.resume()
        }
        
        
        // Allow tapping on photoImageView
        image.userInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewRestaurantViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(animated: Bool) {
        // Watch for keyboard changes
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(animated: Bool) {
        // Unregister from listening to keyboard changes
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func saveRestaurant(sender: AnyObject) {
        
        if let _  = self.restaurant?.id {
            self.restaurant!.name = self.name.text!
            self.restaurant?.location = self.address.text!
            self.restaurant?.category = self.category.text!
            self.restaurant?.photo = self.image.image!
            RestaurantLoader.sharedLoader.updateRestaurantToServer(self.restaurant!)
            self.navigationController?.popViewControllerAnimated(true)
            
        } else {
            self.restaurant = Restaurant(name: self.name.text!)
            self.restaurant?.category = self.category.text!
            self.restaurant?.location = self.address.text!
            self.restaurant?.photo = self.image.image!
            
            self.showLoadingAlert()
            RestaurantLoader.sharedLoader.uploadRestaurantToServer(self.restaurant!) {(restaurant, error) in
                self.dismissViewControllerAnimated(true, completion: nil)
                self.delegate?.viewController(self, didAddRestaurant: restaurant)
            
            }
        }
        
    }

    //    - (void)registerForKeyboardNotifications
    func registerForKeyboardNotifications() {
        //    [[NSNotificationCenter defaultCenter] addObserver:self
        //    selector:@selector(keyboardWasShown:)
        //    name:UIKeyboardDidShowNotification object:nil];
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddNewRestaurantViewController.keyboardWasShown(_:)), name: UIKeyboardDidShowNotification, object: nil)

        //    [[NSNotificationCenter defaultCenter] addObserver:self
        //    selector:@selector(keyboardWillBeHidden:)
        //    name:UIKeyboardWillHideNotification object:nil];

        NSNotificationCenter.defaultCenter()
            .addObserver(self, selector: #selector(AddNewRestaurantViewController.keyboardWillBeHidden(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }

    // Called when the UIKeyboardDidShowNotification is sent.
    //    - (void)keyboardWasShown:(NSNotification*)aNotification
    func keyboardWasShown(aNotification: NSNotification) {
        //    NSDictionary* info = [aNotification userInfo];
        if let info: NSDictionary = aNotification.userInfo {
            //    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
            if let kbSize: CGSize = info.objectForKey(UIKeyboardFrameBeginUserInfoKey)?.CGRectValue().size {
                //    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)

                //    scrollView.contentInset = contentInsets;
                //    scrollView.scrollIndicatorInsets = contentInsets;
                self.scrollView.contentInset = contentInsets;
                self.scrollView.scrollIndicatorInsets = contentInsets;
            }
        }

        //    // If active text field is hidden by keyboard, scroll it so it's visible
        //    // Your app might not need or want this behavior.
        //    CGRect aRect = self.view.frame;
        //    aRect.size.height -= kbSize.height;
        //    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        //    [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
        //    }
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    //    - (void)keyboardWillBeHidden:(NSNotification*)aNotification
    func keyboardWillBeHidden(aNotification: NSNotification) {
        //    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        
        //    scrollView.contentInset = contentInsets;
        //    scrollView.scrollIndicatorInsets = contentInsets;
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        
        if self.restaurant == nil {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func selectImageFromPhotoLibrary(sender: UITapGestureRecognizer) {
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.sourceType = .PhotoLibrary
        
        imagePickerController.delegate = self
        presentViewController(imagePickerController, animated: true, completion: nil)
        
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        image.image = selectedImage
        dismissViewControllerAnimated(true, completion: nil)
        
    }

}
