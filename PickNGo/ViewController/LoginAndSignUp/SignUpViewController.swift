//
//  SignUpViewController.swift
//  PickNGo
//


import UIKit
import Firebase

class SignUpViewController: UIViewController {
  

    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var ref: FIRDatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func createAction(sender: AnyObject) {
      
        if self.emailTextField.text!.characters.count < 8  {
            showSimpleAlert("Invalid", message:  "Please enter email address")
        } else if self.passwordTextField.text!.characters.count < 8 {
            showSimpleAlert("Invalid", message:  "Password must be greater than 8 characters")
        } else if self.userNameTextField.text!.characters.count < 5 {
            showSimpleAlert("Invalid", message:  "Username must be greater than 5 characters")
        } else {
            self.ref = FIRDatabase.database().reference()
            self.createButton.hidden = true
            self.loadingActivityIndicator.startAnimating()
            FIRAuth.auth()?.createUserWithEmail(self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
                
                self.loadingActivityIndicator.stopAnimating()
                self.createButton.hidden = false
                
                
                let currentuser = User(name: self.userNameTextField.text!)
                currentuser.contactNumber = self.phoneTextField.text!
                User.currentUser = currentuser
                
                
                if let user = user {
                    // Add user role into firebase database
                    self.ref.child("users").child(user.uid).setValue(["email": user.email!, "userName": self.userNameTextField.text!, "phoneNumber": self.phoneTextField.text!])
                    
                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                } else {
                    print(error?.localizedDescription)
                }
            }
        }

    }}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        }
        return true
    }
}
