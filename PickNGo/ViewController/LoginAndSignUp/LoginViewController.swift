//
//  LoginViewController.swift
//  PickNGo
//

import UIKit
import Firebase
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {

    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordAction: UITextField!
    @IBOutlet weak var signInButton: GIDSignInButton!
    private var activeTextField: UITextField!
    var ref: FIRDatabaseReference!
    
    var emailValidated: Bool {
    return !(usernameText.text!.isEmpty)
    }
    
    var passwordValidated: Bool {
        return !(password.text!.isEmpty)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
//
//        view.addGestureRecognizer(tap)
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        // Uncomment to automatically sign in the user.
        //GIDSignIn.sharedInstance().signInSilently()
        
        // TODO(developer) Configure the sign-in button look/feel
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if let _ = FIRAuth.auth()?.currentUser {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    

    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        

        if let error = error {
            print(error.localizedDescription)
            return
        } else {

            let fullName = user.profile.name
            let email = user.profile.email
            let authentication = user.authentication
            let credential = FIRGoogleAuthProvider.credentialWithIDToken(authentication.idToken,
                                                                         accessToken: authentication.accessToken)
            
            self.loginButton.hidden = true
            self.signInButton.hidden = true
            self.loadingActivityIndicator.startAnimating()
            
            FIRAuth.auth()?.signInWithCredential(credential) { (user, error) in
                
                self.signInButton.hidden = false
                self.loginButton.hidden = false
                self.loadingActivityIndicator.stopAnimating()
                
                if let user = user {
                    
                    self.ref = FIRDatabase.database().reference()
                    // Add user role into firebase database
                    self.ref.child("users").child(user.uid).setValue(["email": email, "userName": fullName])
                    
                    (self.presentingViewController as? PNGTabBarController)?.selectedIndex = 1
                    if let transactionVC: TransactionViewController = ((self.presentingViewController as? PNGTabBarController)?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? TransactionViewController {
                        transactionVC.handleRefresh(transactionVC.refreshControl)
                    }
                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                    
                } else {
                    print(error?.localizedDescription)
                }
                
            }
        }
        // ...
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    @IBAction func googleSignInClick(sender: AnyObject) {
        print("Click")
        self.loadingActivityIndicator.startAnimating()
        self.loginButton.hidden = true
        self.signInButton.hidden = true
    }
    
    @IBAction func usernameAction(sender: AnyObject) {
        usernameText?.resignFirstResponder()
    }

    @IBAction func callLogin(sender: AnyObject) {
        
        self.loginButton.hidden = true
        self.signInButton.hidden = true
        self.loadingActivityIndicator.startAnimating()
        
        FIRAuth.auth()?.signInWithEmail(self.usernameText.text!, password: self.password.text!) { (user, error) in
            
            self.loginButton.hidden = false
            self.signInButton.hidden = false
            self.loadingActivityIndicator.stopAnimating()
            
            if let _ = user {
//                User.currentUser = User(name: self.usernameText.text ?? "")
                self.dismissViewControllerAnimated(true, completion: nil)
            } else {
                self.showSimpleAlert("Error", message: "Unsuccessful login with error message: \(error!.localizedDescription)")
            }
        }
    }

    @IBAction func backAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
}

