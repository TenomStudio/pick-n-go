//
//  ProfileViewController.swift
//  PickNGo
//
//  Created by kimberly on 06/08/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
   
    @IBOutlet weak var uploadImage: UIButton!
    @IBOutlet weak var saveButton: UIButton!
//    @IBOutlet weak var maleButton: UIButton!
//    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var logoutbutton: UIButton!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var phoneNumberTxt: UITextField!
    @IBOutlet weak var addressTxt: UITextField!
    
//    let genderImage: UIImage = UIImage(named: "check_big_icon")!
    var isFemale:Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()        
//        photo.userInteractionEnabled = true
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewRestaurantViewController))
//        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        
     
    }
    
    override func viewDidAppear(animated: Bool) {
        
        nameTxt.text = User.currentUser?.name
        phoneNumberTxt.text = User.currentUser?.contactNumber
    }
//    @IBAction func segmentSwitch(sender: AnyObject) {
//        if let segmentedControl: UISegmentedControl = sender as? UISegmentedControl {
//            if segmentedControl.selectedSegmentIndex == 0 {
//                 isFemale = true
//                // Selected female
//            } else if segmentedControl.selectedSegmentIndex == 1 {
//                // Selected male
//                isFemale = false
//            }
//        }
//        
//    }
  
    
//    
//    @IBAction func selectMale(sender: AnyObject) {
//        femaleButton.setImage(nil, forState: UIControlState.Normal)
//        maleButton.setImage(genderImage, forState: UIControlState.Normal)
//        isFemale = false
//    }
//    @IBAction func selectFemale(sender: UIButton) {
//        femaleButton.setImage(genderImage, forState: UIControlState.Normal)
//        maleButton.setImage(nil, forState: UIControlState.Normal)
//        isFemale = true
//    }

    @IBAction func uploadPicture(sender: AnyObject) {

        
            let imagePickerController = UIImagePickerController()
            
            imagePickerController.sourceType = .PhotoLibrary
            
            imagePickerController.delegate = self
            presentViewController(imagePickerController, animated: true, completion: nil)
            
            
        }
        func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
            
            let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            uploadImage.setImage(selectedImage, forState:UIControlState.Normal)
            uploadImage.layer.cornerRadius = uploadImage.bounds.size.width * 0.5
            dismissViewControllerAnimated(true, completion: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func logout(sender: AnyObject) {
        try! FIRAuth.auth()!.signOut()
        let storyboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
        self.presentViewController(vc, animated: true, completion:nil)
        
    }

    @IBAction func saveAllTheData(sender: AnyObject) {
        
        let currentuser = User(name: self.nameTxt.text!)
        currentuser.contactNumber = self.phoneNumberTxt.text!
        currentuser.address = self.addressTxt.text!
//        currentuser.gender = isFemale ? User.Gender.Female : User.Gender.Male
        currentuser.image = self.uploadImage.currentImage
        
        
        User.currentUser = currentuser
        

        let alertController = UIAlertController(title: "DATA SAVE!", message: "", preferredStyle: .Alert)
        
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            // ...
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
        
    }
    }





