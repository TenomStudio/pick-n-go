//
//  PickNGo
//
//  Created by PC Chan on 16/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit
import Firebase

class RestaurantListingViewController: UIViewController {
    
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var restaurantTableView: UITableView!
    
    var restaurants: [Restaurant] = []
    var food: [Food] = []
    var ref: FIRDatabaseReference!
    var session: NSURLSession!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(RestaurantListingViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.restaurantTableView.addSubview(self.refreshControl)
        
        if let _ = FIRAuth.auth()?.currentUser {
            self.ref = FIRDatabase.database().reference()
            
            RestaurantLoader.sharedLoader.readRestaurantsFromServer { (restaurants, error) in
                
                if restaurants.count > 0 {
                    self.restaurants = restaurants
                    self.restaurantTableView.reloadData()
                } else {
                    self.showSimpleAlert("Error", message: error?.localizedDescription)
                }
            }
            
        } else {
            let storyboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as UIViewController
            self.presentViewController(vc, animated: true, completion: nil)
        }
  
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let _ = FIRAuth.auth()?.currentUser {
            if restaurants.count == 0 {
                RestaurantLoader.sharedLoader.readRestaurantsFromServer { (restaurants, error) in
                    self.restaurants = restaurants
                    self.restaurantTableView.reloadData()
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAddRestaurantVC" {
            let destNC = segue.destinationViewController as! UINavigationController
            let destVC = destNC.topViewController as! AddNewRestaurantViewController
            destVC.delegate = self
        } 
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        
        RestaurantLoader.sharedLoader.readRestaurantsFromServer { (restaurants, error) in
            self.restaurants = restaurants
            self.restaurantTableView.reloadData()
            refreshControl.endRefreshing()
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let Edit = UITableViewRowAction(style: .Normal, title: "Edit") { action, index in
            print("Edit button tapped")
            let detailRestaurantNC = self.storyboard?.instantiateViewControllerWithIdentifier("AddRestaurantNC") as! UINavigationController
            let detailRestaurantVC = detailRestaurantNC.topViewController as! AddNewRestaurantViewController
            
            let restaurant: Restaurant = self.restaurants[indexPath.row]
            detailRestaurantVC.restaurant = restaurant
            self.navigationController?.pushViewController(detailRestaurantVC, animated: true)
            self.restaurantTableView.setEditing(false, animated: true)
            
        }
        Edit.backgroundColor = UIColor.orangeColor()
        
        let Delete = UITableViewRowAction(style: .Normal, title: "Delete") { action, index in
            print("Delete button tapped")
            RestaurantLoader.sharedLoader.deleteRestaurantFromServer(self.restaurants.removeAtIndex(indexPath.row))
            self.restaurantTableView.reloadData()
        }
        Delete.backgroundColor = UIColor.redColor()
        
        return [Delete, Edit]
    }
    
}

extension RestaurantListingViewController: AddRestaurantDelegate {
    func viewController(vc: AddNewRestaurantViewController, didAddRestaurant restaurant: Restaurant!) {
        self.restaurants.append(restaurant)
        self.dismissViewControllerAnimated(true, completion: nil)
        self.restaurantTableView.reloadData()
    }
}

extension RestaurantListingViewController : UITableViewDataSource {
    func tableView(restaurantTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(restaurantTableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: RestaurantCell = restaurantTableView.dequeueReusableCellWithIdentifier("RestaurantCell") as! RestaurantCell
        let restaurant: Restaurant = self.restaurants[indexPath.row]
        cell.restaurant = restaurant
        return cell
    }
}

extension RestaurantListingViewController: UITableViewDelegate {
    func tableView(restaurantTableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let foodsVC: FoodListingViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FoodListingViewController") as! FoodListingViewController
        
        let foods: [Food] = self.restaurants[indexPath.row].foods
        foodsVC.foods = foods
        self.navigationController?.navigationBar.tintColor = UIColor.yellowColor()
        self.navigationController?.pushViewController(foodsVC, animated: true)
    }
}

