//
//  UserProfileViewController.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 31/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit
import Firebase

class UserProfileViewController: UIViewController {

    @IBOutlet weak var signOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signOutUser(sender: AnyObject) {
        
        try! FIRAuth.auth()!.signOut()
        let storyboard: UIStoryboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
        let initialVC: UIViewController = storyboard.instantiateInitialViewController()!
        
        self.presentViewController((rootViewController: initialVC), animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
