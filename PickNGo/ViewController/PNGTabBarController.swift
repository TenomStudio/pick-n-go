//
//  PNGTabBarController.swift
//  PickNGo
//
//  Created by Learn on 23/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit
import Firebase

class PNGTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Check if user is login, else redirect to login screen
        if let _ = FIRAuth.auth()?.currentUser {
            
        } else {
            self.showLoginScreen()
        }
    }
    
    func showLoginScreen() {
        let storyboard: UIStoryboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
        let initialVC: UIViewController = storyboard.instantiateInitialViewController()!
        
        self.presentViewController(initialVC, animated: false, completion: nil)
    }
}
