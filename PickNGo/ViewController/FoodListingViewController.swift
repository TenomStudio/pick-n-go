//
//  FoodListingViewController.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 29/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit
import Firebase

protocol showFoodListDelegate {
    func viewController(vc: FoodListingViewController, didAddFoods foods: [Food]!)
}

class FoodListingViewController: UIViewController {
    
    @IBOutlet weak var foodTableView: UITableView!
    
    var foods: [Food] = []
    var ref: FIRDatabaseReference!
    var session: NSURLSession!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.foodTableView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAddFoodVC" {
            let destVC: AddFoodViewController = segue.destinationViewController as! AddFoodViewController
            destVC.delegate = self
        }
    }

}

extension FoodListingViewController: AddFoodDelegate {
    func viewController(vc: AddFoodViewController, didAddFood food: Food!) {
        self.foods.append(food)
        self.dismissViewControllerAnimated(true, completion: nil)
        self.foodTableView.reloadData()
    }
}

extension FoodListingViewController: UITableViewDataSource {
    func tableView(foodTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foods.count
    }
    
    func tableView(foodTableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: FoodCell = foodTableView.dequeueReusableCellWithIdentifier("FoodCell") as! FoodCell
        
        let food: Food = self.foods[indexPath.row]
        cell.food = food
        return cell
    }
}
