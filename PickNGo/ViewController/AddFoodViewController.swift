//
//  AddFoodViewController.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 30/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit

protocol AddFoodDelegate {
    func viewController(vc: AddFoodViewController, didAddFood food: Food!)
}

class AddFoodViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var food: Food?
    var delegate: AddFoodDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func saveFood(sender: AnyObject) {
        self.food = Food(name: self.nameTextField.text!)
        self.delegate?.viewController(self, didAddFood: self.food)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
