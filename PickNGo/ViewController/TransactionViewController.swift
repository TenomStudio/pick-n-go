//
//  TransactionViewController.swift
//  PickNGo
//
//  Created by PM Developer on 09/08/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import UIKit
import Firebase

class TransactionViewController: UIViewController {
    var transactions: [Transaction] = []
    var ref: FIRDatabaseReference!
    @IBOutlet weak var transactionTableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(TransactionViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        self.transactionTableView.addSubview(self.refreshControl)
        if let _ = FIRAuth.auth()?.currentUser {
            self.ref = FIRDatabase.database().reference()
            
            TransactionLoader.service.getOrderedItem { (transactions, error) in
                
                if transactions.count > 0 {
                    self.transactions = transactions
                    self.transactionTableView.reloadData()
                } else {
                    self.showSimpleAlert("Error", message: error?.localizedDescription)
                }
               
            }
            
        }
    }
    func handleRefresh(refreshControl: UIRefreshControl) {
        
        TransactionLoader.service.getOrderedItem { (transactions, error) in
            self.transactions = transactions
            self.transactionTableView.reloadData()
            refreshControl.endRefreshing()
        }

    }
}

extension TransactionViewController : UITableViewDataSource {
    func tableView(transactionTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func tableView(transactionTableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: TransactionCell = transactionTableView.dequeueReusableCellWithIdentifier("TransactionCell") as! TransactionCell
        let transaction: Transaction = self.transactions[indexPath.row]
        cell.transaction = transaction
        return cell
    }
}
