//
//  Restaurant.swift
//  PickNGo
//
//  Created by PC Chan on 16/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Restaurant {
    var id: String?
    var name: String
    var location: String?
    var url: String?
    var category: String?
    var photo: UIImage?
    var isDeleted: String?
    var foods: [Food] = []
    var restaurantReviews: [Review] = []
    let ref: FIRDatabaseReference?
    
    /// Initialize restaurant object using name parameter
    init?(name: String) {
        self.name = name
        self.ref = nil
        
        if name.stringByReplacingOccurrencesOfString(" ", withString: "").isEmpty {
            return nil
        }
    }
    
    /// Initialize restaurant object using firebase snapshot
    init(snapshot: FIRDataSnapshot) {
        self.name = snapshot.value!["name"] as! String
        self.url = snapshot.value!["url"] as? String
        self.category = snapshot.value!["category"] as? String
        self.location = snapshot.value!["address"] as? String
        self.id = snapshot.key
        
        if let foodsDict: [String: AnyObject] = snapshot.value!["foods"] as? [String: AnyObject] {
            for item in foodsDict {
                let food = Food(name: item.1["name"] as! String)!
                food.url = item.1["url"] as? String
                food.maxPrice = item.1["price"] as? Float
                food.id = item.0
                self.foods.append(food)
            }
        }
        
        ref = snapshot.ref
    }
    
    convenience init() {
        self.init(name: "")!
    }
    
    /// This is a function to return average rating for the restaurant
    /// - Returns: Rating
    func averageRating() -> Double {
        if restaurantReviews.count == 0 {
            return 0.0
        }
        
        var average: Double = 0.0
        for review in self.restaurantReviews {
            average += Double(review.rating)
        }
        return average / Double(restaurantReviews.count)
    }
}
