//
//  User.swift
//  PickNGo
//
//  Created by PC Chan on 19/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import UIKit

class User: NSObject, NSCoding {
    
    enum Gender {
        case Male, Female
    }
    
    static var currentUser: User?
    
    var name: String
    var gender: Gender?
    var contactNumber: String?
    var image: UIImage?
    var email: String?
    var address: String?
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.name, forKey: "UserName")
        keyedArchiver.encodeObject(self.email, forKey: "UserEmail")
        keyedArchiver.encodeObject(self.contactNumber, forKey: "UserContact")
        keyedArchiver.encodeObject(self.address,forKey:"UserAddress")
        
        if self.image != nil {
            let imageData: NSData? = UIImagePNGRepresentation(self.image!)
            keyedArchiver.encodeObject(imageData, forKey: "UserProfilePhoto")
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let name: String = keyUnarchiver.decodeObjectForKey("UserName") as! String
        self.init(name: name)
        
        self.email = keyUnarchiver.decodeObjectForKey("UserEmail") as? String
        self.contactNumber = keyUnarchiver.decodeObjectForKey("UserContact") as? String
        
        if let imageData = keyUnarchiver.decodeObjectForKey("UserProfilePhoto") as? NSData {
            self.image = UIImage(data: imageData)
        }
        
    }
    init(name: String) {
        self.name = name
        super.init()
    }
}
