//
//  Food.swift
//  PickNGo
//
//  Created by PC Chan on 16/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import UIKit

class Food {
    var id: String?
    var name: String
    var url: String?
    var type: String?
    var category: String?
    var photo: UIImage?
    var isDeleted: Bool?
    var maxPrice: Float?
    var minPrice: Float?
    
    init?(name: String) {
        self.name = name
    }
    
}
