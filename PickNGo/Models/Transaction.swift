//
//  Transaction.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 10/08/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import Firebase

class Transaction {
    var id: String?
    var buyer: String?
    var foodName: String?
    var quantity: Int?
    var price: Float?
    var orderDate: NSDate?
    var timeAgo: String?
    
    // Normal initialize
    init() {}
    
    // Initialize transaction object using firebase snapshot
    init(snapshot: FIRDataSnapshot) {
        self.id = snapshot.key
        self.buyer = snapshot.value!["buyer"] as? String
        self.foodName = snapshot.value!["foodName"] as? String
        self.quantity = snapshot.value!["quantity"] as? Int
        self.price = snapshot.value!["price"] as? Float
        
        // Date formatter
        let dateString = snapshot.value!["orderDate"] as? String
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZZ"
        let date = dateFormatter.dateFromString(dateString!)

        self.orderDate = date
        self.timeAgo = date!.timeAgoSince()
    }
    
    
}