//
//  RestaurantCell.swift
//  PickNGo
//
//  Created by PC Chan on 19/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import UIKit

// Restaurant Cell
class RestaurantCell: UITableViewCell {
    
    var food: Food! {
        didSet {
            restaurantImageView.image = food.photo
            restaurantName.text = food.name
        }
    }
    @IBOutlet private weak var restaurantImageView: UIImageView!
    @IBOutlet private weak var restaurantName: UILabel!
}