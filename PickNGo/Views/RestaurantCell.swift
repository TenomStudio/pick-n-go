//
//  RestaurantCell.swift
//  PickNGo
//
//  Created by PC Chan on 21/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import UIKit

class RestaurantCell: UITableViewCell {
    @IBOutlet private weak var restaurantImageView: UIImageView!
    @IBOutlet private weak var restaurantName: UILabel!
    @IBOutlet weak var category: UILabel!
    

    var loadImageTask: NSURLSessionDataTask?
    
    var restaurant: Restaurant! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        self.category.text = self.restaurant.category
        self.restaurantName.text = self.restaurant.name
        if let photo = self.restaurant.photo {
            self.restaurantImageView.image = photo
        } else if let photoURL = self.restaurant.url {
            let url = NSURL(string: photoURL)!
            self.loadImageTask?.cancel()
            self.loadImageTask = RestaurantLoader.sharedLoader.loadImageTask(url, completionBlock: { (image, error) in
                if let image = image {
                    self.restaurantImageView.image = image
                }
            })
            self.loadImageTask?.resume()
        }
    }

}