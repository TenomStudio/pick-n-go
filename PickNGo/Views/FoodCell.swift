//
//  FoodCell.swift
//  PickNGo
//
//  Created by Chan Phang chuen on 29/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class FoodCell: UITableViewCell {
    
    @IBOutlet private weak var foodImageView: UIImageView!
    @IBOutlet private weak var foodName: UILabel!
    @IBOutlet private weak var foodPrice: UILabel!
    @IBOutlet weak var foodQuantity: UILabel!
    
    @IBOutlet weak var type: UILabel!
    
    @IBOutlet weak var orderNowButton: UIButton!
    @IBOutlet weak var orderedLabel: UILabel!
    var loadImageTask: NSURLSessionDataTask?
    
    
    var food: Food! {
        didSet {
            self.orderedLabel.hidden = true
        
            updateDisplay()
        }
    }
    @IBAction func orderPlaced(sender: AnyObject) {
        self.orderNowButton.hidden = true
        self.orderedLabel.hidden = false
        
        if let user = FIRAuth.auth()?.currentUser {
            let transaction: Transaction = Transaction()
            transaction.buyer = user.displayName ?? ""
            transaction.foodName = self.foodName!.text
            transaction.price = Float(self.foodPrice.text!)
            transaction.quantity = Int(self.foodQuantity.text!)
            transaction.orderDate = NSDate()
            
        TransactionLoader.service.addOrderedItem(transaction)
        }
        

    }
    
    @IBAction func quantityStepper(sender: UIStepper) {
        self.foodQuantity.text = Int(sender.value).description
    }
    
    func updateDisplay() {
        self.type.text = self.food.type
        self.foodName.text = self.food.name
        self.foodPrice.text = String(self.food.maxPrice!)
        if let photo = self.food.photo {
            self.foodImageView.image = photo
        } else if let photoURL = self.food.url {
            let url = NSURL(string: photoURL)!
            self.loadImageTask?.cancel()
            self.loadImageTask = FoodLoader.sharedLoader.loadImageTask(url, completionBlock: { (image, error) in
                if let image = image {
                    self.foodImageView.image = image
                }
                
            })
            
            }

            self.loadImageTask?.resume()
        }
    }


