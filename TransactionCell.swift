//
//  TransactionCell.swift
//  
//
//  Created by PM Developer on 09/08/2016.
//
//

import Foundation
import UIKit

class TransactionCell: UITableViewCell {
    @IBOutlet private weak var foodName: UILabel!
    @IBOutlet weak var foodQuantity: UILabel!
    @IBOutlet weak var foodDateTime: UILabel!
    @IBOutlet weak var foodPrice: UILabel!
    
    
    var transaction: Transaction! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        self.foodName.text = self.transaction.foodName!
        self.foodPrice.text = String(self.transaction.price!)
        self.foodQuantity.text = String(self.transaction.quantity!)
        self.foodDateTime.text = self.transaction.timeAgo!
    }
}
    