//
//  PokeAPITest.swift
//  PickNGo
//
//  Created by PC Chan on 02/08/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import XCTest

class PokeAPITest: XCTestCase {
    
    var pokemons: [Pokemon] = []
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPokeAPI() {
        let expectation = self.expectationWithDescription("Load Task")
        let session: NSURLSession = NSURLSession.sharedSession()
        let url: NSURL = NSURL(string: "https://pokeapi.co/api/v2/pokemon/?limit=1000")!
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (data: NSData?, response:NSURLResponse?, error: NSError?) in
           print(data!)
            let _: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            let json: [String: AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String: AnyObject]
            if let resultsArray : [ [ String: AnyObject] ] = json["results"] as? [[String: AnyObject]] {
                for pokeData in resultsArray {
                    let name: String = pokeData["name"] as! String
                    let pokemon: Pokemon = Pokemon(name: name)
                    pokemon.url = pokeData["url"] as? String
                    self.pokemons.append(pokemon)
                }
            }
            
            print(self.pokemons.count)
            expectation.fulfill()
        }
        loadTask.resume()
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }
    

}

class Pokemon {
    var name: String
    var url: String?
    
    init(name: String) {
        self.name = name
    }
}
