//
//  PickNGoTests.swift
//  PickNGoTests
//
//  Created by PC Chan on 16/07/2016.
//  Copyright © 2016 pik. All rights reserved.
//

import XCTest
import Firebase

@testable import PickNGo

class PickNGoTests: XCTestCase {
    
    var email: String!
    var password: String!
    var ref: FIRDatabaseReference!
    var foods: [Food] = []
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        self.ref = FIRDatabase.database().reference()
        self.email = "blackevillee@hotmail.com"
        self.password = "HarloPC"
        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testEmptyNameRestaurant() {        
        let restaurantWithNoName: Restaurant! = Restaurant(name: "    ")
        XCTAssert(restaurantWithNoName == nil, "Restaurant shouldn't have been created with empty name")
    }
    
    func testFirebaseCreateUser() {
        FIRApp.configure()
        let expectation = self.expectationWithDescription("Load Task")
        FIRAuth.auth()?.createUserWithEmail(self.email, password: self.password) { (user, error) in
            if let user = user {
                print(user)
                FIRAuth.auth()?.signInWithEmail(self.email, password: self.password) { (signedUser, error) in
                    if let signedUser = signedUser {
                        // add user role
                        self.ref.child("users").child(signedUser.uid).setValue(["email": user.email!])
                    }
                }
            }
            else {
                XCTAssertNil(user)
                print(error)
            }
            expectation.fulfill()
        }
        self.waitForExpectationsWithTimeout(10, handler: nil)
        
    }
    
    func testFirebaseSignInWithPassword() {
        FIRApp.configure()
        let expectation = self.expectationWithDescription("Load Task")
        FIRAuth.auth()?.signInWithEmail(self.email!, password: self.password!) { (user, error) in
            if let user = user {
                print(user)
                //self.ref.child("users").child(user.uid).setValue(["email": user.email!])
                //let key = self.ref.child("usersObject").childByAutoId().key
                let userFood = ["uid": user.uid,
                                "email": user.email!,
                                "gender" : "male"]
                let childUpdates = ["/userObject/\(user.uid)": userFood]
                self.ref.updateChildValues(childUpdates)
            }
            else {
                XCTAssertNil(user)
                print(error)
            }
            expectation.fulfill()
        }
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testUploadFileToFirebase() {
        FIRApp.configure()
        
        if let user = FIRAuth.auth()?.currentUser {
            let expectation = self.expectationWithDescription("Upload Task")
            let storage = FIRStorage.storage()
            let storageRef = storage.referenceForURL("gs://pickngo-3853e.appspot.com")
            let foodNames: [String] = ["friedMeat","friedVege","mee","rice","springRoll","tart"]
            
            //Add multiple foods into storage and database
            for i in 0..<foodNames.count {
                
                // Get UI image from assets
                let image: UIImage = UIImage(named: foodNames[i])!
                
                // Transform image to NSData
                let data: NSData = UIImagePNGRepresentation(image)!
                
                // Generate unique key for foods
                let key = self.ref.child("foods").childByAutoId().key
                
                // Create firebase storage reference based on image name
                let imagesRef = storageRef.child("images/\(key).jpg")
                
                // Populate data into the ref
                let _ = imagesRef.putData(data, metadata: nil) { metadata, error in
                    
                    // If error, print error description
                    if (error != nil) {
                        print(error?.localizedDescription)
                    } else {
                        
                        // On added image, get image url and add into database
                        let foodImage = ["name": foodNames[i],
                                "url": metadata!.downloadURL()!.absoluteString]
                       
                        
                        // Add foods into food object and userObject
                        let childUpdates = ["/foods/\(key)": foodImage,
                                        "/userObject/\(user.uid)/\(key)": foodImage]
                        self.ref.updateChildValues(childUpdates)
                    }
                    expectation.fulfill()
            }
            
        }
        }
        
        
        
        // Upload the file to firebase storage
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
        
    }
    
    func testReadAllFoods() {
        FIRApp.configure()

        let expectation = self.expectationWithDescription("Read Task")
//        self.ref = FIRDatabase.database().reference()
//        ref.child("foods").observeEventType(.Value, withBlock: { (snapshot) in
//    
//            for child in snapshot.children {
//                let childSnapshot = snapshot.childSnapshotForPath(child.key)
//                let food: Food = Food(name: childSnapshot.value!["name"])!
//                food.url = childSnapshot.value!["url"]
//                self.foods.append(food)
//            }
//            expectation.fulfill()
//        })
        
        self.waitForExpectationsWithTimeout(1000, handler: nil)
    }
    
    
    func testReadFileFromFirebase() {
        FIRApp.configure()
        
        
        let expectation = self.expectationWithDescription("Upload Task")
        let httpsReference = FIRStorage.storage().referenceForURL("https://firebasestorage.googleapis.com/v0/b/pickngo-3853e.appspot.com/o/images%2FfriedMeat.jpg?alt=media&token=7f56d35b-047e-4ec8-ab62-c47bac3d2266")
        
        // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
        httpsReference.dataWithMaxSize(1 * 1024 * 1024) { (data, error) -> Void in
            if (error != nil) {
                // Uh-oh, an error occurred!
            } else {
                // Data for "images/island.jpg" is returned
                let foodImage: UIImage! = UIImage(data: data!)
                print("Done")
            }
            expectation.fulfill()
        }
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }
    
    func testDownloadFileFromFirebase() {
        FIRApp.configure()
        
        let expectation = self.expectationWithDescription("Download Task")
        let storage = FIRStorage.storage()
        let storageRef = storage.referenceForURL("gs://pickngo-3853e.appspot.com")
        let pathReference = storageRef.child("/images/friedMeat.jpg")
        
        // Replace Username <PCChan> to your current machine name
        let localURL: NSURL! = NSURL(string: "file:///Users/PCChan/Desktop/friedMeat.jpg")
        
        let downloadTask = pathReference.writeToFile(localURL) { (URL, error) -> Void in
            if error != nil {
                print(error?.localizedDescription)
            } else {
                print(URL?.absoluteString)
            }
            expectation.fulfill()
            
        }
        self.waitForExpectationsWithTimeout(10, handler: nil)
        
        
    }
    
    func testReadCurrentUser() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let user: User = User(name: "Ivan")
        user.email = "blackevillee@hotmail.com"
        user.image = UIImage(named: "profile")
        user.contactNumber = "0129747421"
        
        let userData: NSData = NSKeyedArchiver.archivedDataWithRootObject(user)
        
        defaults.setObject(userData, forKey: "userData")
        defaults.synchronize()
        
        let readUserData: NSData = defaults.objectForKey("userData") as! NSData
        let readUser: User? = NSKeyedUnarchiver.unarchiveObjectWithData(readUserData) as? User
        
        XCTAssertNotNil(readUser, "This shouldn't be nil")
        XCTAssertEqual(user.name, readUser!.name, "Both should match")
    }
    
    func testReadFileFromDisk() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        //let _: UIImage? = defaults.objectForKey("Image")
        print("Done")
    }
    
    
}
